/// @description 
star = function(_x, _y, _r, _col) constructor {
	x = _x;
	y = _y;
	radius = _r;
	period = random_range(0, 2*PI);
	color = _col;
}

star_array = [];

num_stars = 150;

step = 0;

for (var _i=0; _i<num_stars; _i++) {
	array_push(star_array, new star(irandom_range(0,room_width), irandom_range(0, room_height), random_range(0.1, 2), choose($aaaaaa, c_aqua, c_yellow, $bbbbbb, $dddddd, c_purple)));
}