/// @description 
if (!Game.paused) {
	step++;
	fnc_BackupDrawParams();
	var _n = array_length(star_array);
	for (var _i=0; _i<_n; _i++) {
		var _star = star_array[_i];
		draw_set_alpha(0.5 + 0.5*cos(step/10-_star.period));
		draw_circle_color(_star.x, _star.y, _star.radius, _star.color, _star.color, false);	
	}
	fnc_RestoreDrawParams();
}