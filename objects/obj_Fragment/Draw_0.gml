/// @description 
if (!Game.paused) {	
	var _alpha = alarm[0]/fragment_duration;
	
	draw_sprite_general(sprite_index, image_index, 
						fragment_index_w*fragment_w, 
						fragment_index_h*fragment_h, 
						fragment_w, 
						fragment_h, 
						x, y, 1, 1, image_angle, c_white, c_white, c_white, c_white, _alpha);
}