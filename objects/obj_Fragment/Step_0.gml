/// @description 
if (!Game.paused) {
	
	if (created) {	
		image_angle = image_angle+fragment_rotation;
		
		x = x + lengthdir_x(fragment_speed, fragment_direction);
		y = y + lengthdir_y(fragment_speed, fragment_direction);
		if (alarm[0] <= 0) {
			instance_destroy();
		}
	}
}