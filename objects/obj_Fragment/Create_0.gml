/// @description 
fragment_direction = irandom_range(0, 360);
fragment_speed = random_range(2, 4);
fragment_rotation = irandom_range(1, 5);
fragment_duration = 120;
alarm[0] = fragment_duration;
created = false;