/// @description Draw agent
if (!Game.paused) {
	
	if (timeout) {
		var _id = instance_nearest(x, y, obj_Player);
	
		if (hp > 0) {
			// Maintain constant distance from player
			if (y - _id.y < 8*TILE_SIZE) {
				if (abs(y-_id.y) > 1) {
					y = lerp(y, _id.y+8*TILE_SIZE, 0.1);
				}
			}
	
			if (abs(x-_id.x) > 1) {
				x = lerp(x, _id.x, 0.05);
			}
	
	
			// Shoot
			if (enemy_number == 2) {
				if (Game.active_player != noone && alarm[1] == -1) {
					var _id = instance_create_layer(x, y-TILE_SIZE/2, "lyr_Instances", obj_Bullet);
					with (_id) {
						shot_by = other.id;
						bullet_angle = 90;
						bullet_speed = 1.5;
						image_index = 1;
					}
					alarm[1] = 30;
				}
			}
			else {
				if (Game.active_player != noone && alarm[1] == -1) {
					var _id = instance_create_layer(x-TILE_SIZE/3, y-TILE_SIZE/2, "lyr_Instances", obj_Bullet);
					with (_id) {
						shot_by = other.id;
						bullet_angle = 90;
						bullet_speed = 1.7;
						image_index = 1;
					}
					var _id = instance_create_layer(x+TILE_SIZE/3, y-TILE_SIZE/2, "lyr_Instances", obj_Bullet);
					with (_id) {
						shot_by = other.id;
						bullet_angle = 90;
						bullet_speed = 1.7;
						image_index = 1;
					}
					alarm[1] = 60;
				}
			}
	
	
			// Change angle
			if (x>xprevious) {			
				var _target = -40;
			}
			else if (x<xprevious) {
				var _target = 40
			}
			else {
				var _target = 0;	
			}
			
			var _curve = animcurve_get_channel(CSSTransitions, "ease-in-cubic");
			var _amount = animcurve_channel_evaluate(_curve, min(steps/60, 1));		
			image_angle = lerp(image_angle, _target, _amount);
		}
		
		// Destroy outside of view or hp nonpositive
		var _top = camera_get_view_y(VIEW) - 2*TILE_SIZE;
		var _bottom = camera_get_view_y(VIEW) + camera_get_view_height(VIEW) + 2*TILE_SIZE;
		if (x < 0 || x > room_width || y < _top || y > _bottom) {
			instance_destroy();
		}
		event_inherited();
	}
}