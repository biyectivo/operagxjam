/// @description 
if (!Game.paused && hp <= 0) {
	if (alarm[3] == -1) { // create fragments
		var _num_fragments = 6;
		for (var _i=0; _i<_num_fragments; _i++) {
			for (var _j=0; _j<_num_fragments; _j++) {
				var _id = instance_create_layer(x, y, "lyr_Instances", obj_Fragment);
				with (_id) {
					sprite_index = spr_Browsers;
					image_index = other.enemy_number;
					fragment_w = sprite_width/_num_fragments;
					fragment_h = sprite_height/_num_fragments;
					fragment_index_w = _i;
					fragment_index_h = _j;
					event_perform(ev_other, ev_user0);
				}
			}
		}
		alarm[3] = obj_Fragment.fragment_duration+1;
		if (Game.option_value[? "Sounds"]) {
			audio_play_sound(snd_Explosion, 1, false);
		}
		instance_destroy();
	}	
}