/// @description 
if (!Game.paused) {
	//if (live_call()) return live_result;
	fnc_BackupDrawParams();
	draw_set_color($666666);
	var _x = x * window_get_width()/room_width;
	var _y = y-camera_get_view_y(VIEW)-TILE_SIZE/2-4;
	draw_set_halign(fa_center);
	draw_set_valign(fa_bottom);
	draw_set_font(fnt_HUD);
	draw_text(_x, _y, string("P"+string(player_number+1)));
	var _y = y - camera_get_view_y(VIEW)+TILE_SIZE+24;
	/*draw_set_font(fnt_Icons);
	draw_text_transformed(_x, _y, ICON_DOWNARROW, 0.25, 0.25, 0);	*/
	fnc_RestoreDrawParams();
}