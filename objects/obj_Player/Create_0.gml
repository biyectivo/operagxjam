/// @description Create player

// General
name = "Player";
controllable = true;
player_number = 0;
animation_dirs = 4;

// Stats

max_fly_haccel = 0.15;
max_fly_hdecel = 0.20;
max_fly_hspeed = 4;

current_fly_hspeed = 0;
current_fly_vspeed = 0;

descent_speed = 1.2;
max_descent_speed = 6;

multiplier = 1;

max_hp = 1;
hp = max_hp;

base_xscale = 1;

// Animation	
animation_startindices = ds_map_create();
animation_lengths = ds_map_create();
animation_spacings = ds_map_create();
animation_speeds = ds_map_create();

// Beginning sprite frame index for each state
animation_startindices[? "Idle"] = 0;
animation_startindices[? "Move"] = 0;
animation_startindices[? "Die"] = 0;

// Length in frames of animation for each state
animation_lengths[? "Idle"] = 4;
animation_lengths[? "Move"] = 4;
animation_lengths[? "Die"] = 1;

// Spacing between frames of animation for each state (default = 1)
animation_spacings[? "Idle"] = 1;
animation_spacings[? "Move"] = 1;
animation_spacings[? "Die"] = 1;

// Animation speed for each frame - lower number means higher speed
animation_speeds[? "Idle"] = 15;
animation_speeds[? "Move"] = 15;
animation_speeds[? "Die"] = 0;


steps_in_state = 0;


/*
occluder       = new BulbDynamicOccluder(obj_BulbRenderer.lighting);
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;

var _l = -0.5*sprite_get_width(sprite_index);
var _t = -0.5*sprite_get_height(sprite_index);
var _r =  0.5*sprite_get_width(sprite_index);
var _b =  0.5*sprite_get_height(sprite_index);

//Use clockwise definitions!
occluder.AddEdge(_l, _t, _r, _t); //Top
occluder.AddEdge(_r, _t, _r, _b); //Right
occluder.AddEdge(_r, _b, _l, _b); //Bottom
occluder.AddEdge(_l, _b, _l, _t); //Left
*/