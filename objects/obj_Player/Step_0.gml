/// @description FSM execution
if (!Game.paused && initialized) {
	last_state = state;

	// Execute current state
	if (Game.debug) {
		show_debug_message("["+Game.game_title+"] "+string(event_type)+" "+name+": "+state+" / "+string(image_index));
	}

	if (script_exists(asset_get_index("fnc_"+name+"FSM_"+state))) {
		script_execute(asset_get_index("fnc_"+name+"FSM_SmoothRotate"));		
		script_execute(asset_get_index("fnc_"+name+"FSM_"+state));		
	}
	
}

/*
occluder.x     = x;
occluder.y     = y;
occluder.angle = image_angle;
*/