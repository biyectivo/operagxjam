/// @description Initialize agent

event_inherited(); // Initialize true

//sprite_index = asset_get_index("spr_Player_"+string(player_number));

// State
state = "Idle";
last_state = state;
animation_name = state;
facing = FACING.EAST;


// Animation
animation_frame_count = 0; // the ith frame of the animation
animation_frame = 0; // the actual frame displayed on the ith frame of animation
reset_animation = false;
script_execute(asset_get_index("fnc_"+name+"FSM_Animate"), true);
alarm[0] = animation_speeds[? animation_name];

// Reset input
player_horizontal_input = 0;
player_vertical_input = 0;
	