/// @description Draw agent
if (!Game.paused && initialized) {	
	fnc_BackupDrawParams();
	
	draw_sprite_ext(spr_Browsers, player_number, x, y, 1,1, image_angle, c_white, 1);
	//draw_sprite_ext(spr_UFO, 0, x, y, 1, 1, 0, c_white, 1);
	draw_self();
	
	if (Game.debug) {
		draw_set_alpha(0.7);
		draw_set_color(c_red);
		draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, false);
		draw_set_halign(fa_center);
		draw_set_valign(fa_middle);
		draw_text(x, y-16, "Index: "+string(image_index));
		
		draw_circle_color(bbox_left, bbox_top, 1, c_yellow, c_yellow, false);
		draw_circle_color(bbox_right, bbox_top, 1, c_yellow, c_yellow, false);
		draw_circle_color(bbox_left, bbox_bottom, 1, c_yellow, c_yellow, false);
		draw_circle_color(bbox_right, bbox_bottom, 1, c_yellow, c_yellow, false);
		
		draw_circle_color(x, bbox_top, 1, c_aqua, c_aqua, false);
		draw_circle_color(x, bbox_bottom, 1, c_aqua, c_aqua, false);
		draw_circle_color(bbox_left, y, 1, c_aqua, c_aqua, false);
		draw_circle_color(bbox_right, y, 1, c_aqua, c_aqua, false);		
	}
	
	fnc_RestoreDrawParams();
}