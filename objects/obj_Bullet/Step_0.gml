/// @description 
if (!Game.paused) {

	x = x + lengthdir_x(bullet_speed, bullet_angle);
	y = y + lengthdir_y(bullet_speed, bullet_angle);

	// Destroy outside of view
	var _top = camera_get_view_y(VIEW) - 2*TILE_SIZE;
	var _bottom = camera_get_view_y(VIEW) + camera_get_view_height(VIEW) + 2*TILE_SIZE;
	if (x < 0 || x > room_width || y < _top || y > _bottom) {
		instance_destroy();
	}
	
}