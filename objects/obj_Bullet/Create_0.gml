/// @description 
shot_by = noone;
bullet_angle = 0;
bullet_speed = 3;

if (Game.option_value[? "Sounds"]) {			
	audio_play_sound(snd_Laser, 1, false);
}