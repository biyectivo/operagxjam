/// @description 
if (!Game.paused) {
	if (Game.active_player != noone) {
		var _tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tile_World"));
		var _row = tilemap_get_cell_y_at_pixel(_tilemap, Game.active_player.x, Game.active_player.y);
		
		// Wifi
		if (_row > 20 && instance_number(obj_Wifi) <= Game.num_players-1) {
			var _r = random(1);
			if (_r < 0.08) {
				var _x;
				var _y;
				_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);
				_y = camera_get_view_y(VIEW)+camera_get_view_height(VIEW);
				while (Game.world[_y div TILE_SIZE][_x div TILE_SIZE] == 1) {
					_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);	
				}
				var _id = instance_create_layer(_x, _y, "lyr_Instances", obj_Wifi);
			}
		}
		
		// Enemies
		var _max_concurrent_enemies = Game.num_players-1 + min(floor(_row/100), 2);
		if (_row > 50 && instance_number(cls_Enemy) < _max_concurrent_enemies) {
			
			var _r = random(1);
			if (_r < 0.05 + min(0.03, 0.01 * floor(_row/10))) {
				
				var _r = random(1);
				
				if (_r < 0.4) {
					// Bullet shooter
					if (instance_number(obj_EnemyBulletShooter) <= Game.num_players-1) {	
						var _x;
						var _y;
						_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);
						_y = camera_get_view_y(VIEW)+camera_get_view_height(VIEW);
						while (Game.world[_y div TILE_SIZE][_x div TILE_SIZE] == 1) {
							_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);	
						}
						var _id = instance_create_layer(_x, _y, "lyr_Instances", obj_EnemyBulletShooter);	
					}
				}
				else if (_r < 0.7) {
		
					// Kamikaze
					if (instance_number(obj_EnemyKamikaze) <= Game.num_players-1) {					
						var _x;
						var _y;
						_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);
						_y = camera_get_view_y(VIEW)+camera_get_view_height(VIEW);
						while (Game.world[_y div TILE_SIZE][_x div TILE_SIZE] == 1) {
							_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);	
						}
						var _id = instance_create_layer(_x, _y, "lyr_Instances", obj_EnemyKamikaze);				
					}
				}
				else {
		
					// Bullet hell
					if (instance_number(obj_EnemyBulletHellShooter) <= Game.num_players-1) {
						var _x;
						var _y;
						_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);
						_y = camera_get_view_y(VIEW)+camera_get_view_height(VIEW);
						while (Game.world[_y div TILE_SIZE][_x div TILE_SIZE] == 1) {
							_x = irandom_range(TILE_SIZE, room_width-TILE_SIZE);	
						}
						var _id = instance_create_layer(_x, _y, "lyr_Instances", obj_EnemyBulletHellShooter);		
					}
				}
			}
		}
	}
}