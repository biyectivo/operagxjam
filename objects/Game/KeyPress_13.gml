/// @description Pause
if (room == room_Game_1) {
	if ((!Game.lost && Game.paused) || Game.lost) {			
		game_restart();
	}
}