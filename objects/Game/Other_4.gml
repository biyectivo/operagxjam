// Enable views and set up graphics

view_enabled = true;
view_visible[0] = true;
fnc_SetGraphics();

// Stop all sounds and set up sound volume
audio_stop_all();
audio_master_gain(option_value[? "Volume"]);	


if (room == room_Game_1) {
		
	// Define pathfinding grid
		
	grid = mp_grid_create(0, 0, room_width/GRID_RESOLUTION, room_height/GRID_RESOLUTION, GRID_RESOLUTION, GRID_RESOLUTION);
				
	// Add collision tiles to grid			
	for (var _col = 0; _col < room_width/GRID_RESOLUTION; _col++) {
		for (var _row = 0; _row < room_height/GRID_RESOLUTION; _row++) {				
			if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _col*GRID_RESOLUTION, _row*GRID_RESOLUTION) != 0) {
				mp_grid_add_cell(grid, _col, _row);
			}
		}
	}
	
	
	// Initialize particle system and emitters
		
	particle_system = part_system_create_layer("lyr_Particles", true);
	particle_emitter_fire = part_emitter_create(particle_system);
	
	// (Re)initialize game start variables		
	fnc_InitializeGameStartVariables();
	
	
	// Instantiate players
	
	var _offset = (num_players == 1 ? 0 : 100);
	var _player1 = instance_create_layer(room_width/2+_offset, 0, "lyr_Instances", obj_Player);
	with (_player1) {		
		player_number = 0;				
		event_perform(ev_other, ev_user0);	
	}
	
	if (num_players ==2) {	
		var _player2 = instance_create_layer(room_width/2-_offset, 0, "lyr_Instances", obj_Player);
		with (_player2) {		
			player_number = 1;		
			event_perform(ev_other, ev_user0);	
		}
	}
	
	
	
	// Play music	
	if (option_value[? "Music"]) {
		music_sound_id = audio_play_sound(snd_Music, 1, true);
	}
	
	fnc_GenerateWorld();
	
	
	// High score
	if (!file_exists("high_score.dat")) {
		fnc_UpdateHighScore(num_players, true); // force creation of file
	}
	else {// Read high score file
		var _fid = file_text_open_read("high_score.dat");
		var _str = file_text_readln(_fid);
		var _split = fnc_StringToList(_str, ";");
		
		var _split_player_hash = fnc_StringToList(_split[|0], "|");
		var _split_player = fnc_StringToList(_split_player_hash[|0], " ");
		
		// 1 Player
		if (sha1_string_utf8(SCOREBOARD_SALT+_split_player[|0]+" "+_split_player[|1]+" "+_split_player[|2]+" "+_split_player[|3]+" "+_split_player[|4]) == _split_player_hash[|1]) {
			high_score_1p = real(_split_player[|3]);
			high_score_distance_1p = real(_split_player[|4]);
			high_score_datetime_string_1p = _split_player[|0]+" "+_split_player[|1];
		}
		else {
			high_score_1p = -1;
			high_score_distance_1p = -1;			
		}
		
		// 2 Player
		var _split_player_hash = fnc_StringToList(_split[|1], "|");
		var _split_player = fnc_StringToList(_split_player_hash[|0], " ");
		
		if (sha1_string_utf8(SCOREBOARD_SALT+_split_player[|0]+" "+_split_player[|1]+" "+_split_player[|2]+" "+_split_player[|3]+" "+_split_player[|4]) == _split_player_hash[|1]) {
			high_score_2p = real(_split_player[|3]);
			high_score_distance_2p = real(_split_player[|4]);
			high_score_datetime_string_2p = _split_player[|0]+" "+_split_player[|1];
		}
		else {
			high_score_2p = -1;
			high_score_distance_2p = -1;			
		}
		file_text_close(_fid)
	}
	
}
