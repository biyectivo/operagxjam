//*****************************************************************************
// Global variable declarations
//*****************************************************************************

debug = false;
//username = "Player";

//*****************************************************************************
// Randomize
//*****************************************************************************

randomize();

//*****************************************************************************
// Set camera target
//*****************************************************************************

camera_target = obj_Player;
camera_shake = false;
camera_smoothness = 0.08;

//*****************************************************************************
// Menu item definitions
//*****************************************************************************

menu_items = array_create(4);
menu_items[0] = "Start game";
menu_items[1] = "Help";
menu_items[2] = "Options";
menu_items[3] = "Credits";
menu_items[4] = "Quit";

option_items = array_create(4);
option_items[0] = "Music";
option_items[1] = "Sounds";
option_items[2] = "Volume";
option_items[3] = "Fullscreen";
option_items[4] = "Controls";
option_items[5] = "Name";

control_indices = [];
control_names = ds_map_create();

control_indices[0] = "left";
control_indices[1] = "right";
control_indices[2] = "down";

controls = [];
controls[0] = ds_map_create();
controls[1] = ds_map_create();

controls[0][? "left"] = vk_left;
controls[0][? "right"] = vk_right;
controls[0][? "down"] = vk_down;

controls[1][? "left"] = ord("A");
controls[1][? "right"] = ord("D");
controls[1][? "down"] = ord("S");

control_names[? "left"] = "Move Left";
control_names[? "right"] = "Move Right";
control_names[? "down"] = "Special";

name_being_modified = false;

wait_for_input = false;
key_being_remapped = noone;

option_type = ds_map_create();
ds_map_add(option_type, option_items[0], "toggle");
ds_map_add(option_type, option_items[1], "toggle");
ds_map_add(option_type, option_items[2], "slider");
ds_map_add(option_type, option_items[3], "toggle");
ds_map_add(option_type, option_items[4], "");
ds_map_add(option_type, option_items[5], "input");

option_value = ds_map_create();
ds_map_add(option_value, option_items[0], true);
ds_map_add(option_value, option_items[1], true);
ds_map_add(option_value, option_items[2], 1.0);
ds_map_add(option_value, option_items[3], false);
ds_map_add(option_value, option_items[4], noone);
ds_map_add(option_value, option_items[5], "Player");

credits[0] = "[fnt_Menu][fa_middle][fa_center][c_white]2021[c_yellow]Programming: [c_white]biyectivo";
credits[1] = "[fa_middle][fa_center][c_white][c_white](Jose Alberto Bonilla Vera)";

game_title = "Game1";
scoreboard_game_id = "Game1";

primary_gamepad = -1;
start_drag_drop = false;

// Particle system and emitter variables - define to avoid not set
particle_system = noone;
particle_emitter_fire = noone;

pause_screenshot = noone;

//*****************************************************************************
// Data structures
//*****************************************************************************
grid = noone;

fnc_InitializeGameStartVariables();

num_players = 1;



gml_pragma("PNGCrush");