/*if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}*/


// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

show_debug_overlay(debug);

// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {		
		active_player = fnc_GetFirstAlivePlayer();
		if (active_player != noone) {
			// Generate endless
			fnc_AddEndless();
			
			// Score
			var _tilemap = layer_tilemap_get_id(layer_get_id("lyr_Tile_World"));
			var _row = tilemap_get_cell_y_at_pixel(_tilemap, active_player.x, active_player.y);
			score_distance = _row;
			with (obj_Player) {
				multiplier = 1 + floor(_row/10)*0.08;
			}
			
			//show_debug_message("Multiplier: "+string(active_player.multiplier)+" depth = "+string(_row));
			//show_debug_message("Enemies: "+string(instance_number(cls_Enemy))+" Bullets: "+string(instance_number(cls_Projectile))+" Wifi: "+string(instance_number(obj_Wifi))+" Total: "+string(instance_number(all)));
		}
		total_score = score_distance + score_bonus;
		
		////if (live_call()) return live_result;

	}
	
}
