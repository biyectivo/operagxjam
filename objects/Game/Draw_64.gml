
fnc_BackupDrawParams();

switch (room) {
	
	default:		
		if (Game.paused) {
			if (Game.lost) {
				instance_deactivate_all(true);
				fnc_DrawYouLost();
			}
			else {				
				fnc_DrawPauseMenu();
			}
		}
		else {			
			fnc_DrawHUD();
		}
		break;
}

// Draw debug overlay on top of everything else
if (debug) {
	fnc_DrawDebug();
}

fnc_RestoreDrawParams();

