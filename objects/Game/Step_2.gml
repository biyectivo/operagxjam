//*****************************************************************************
// Handle camera
//*****************************************************************************


// Fullscreen has changed (either via ALT+TAB or F10 on HTML5 or via menu)
if (fullscreen_change) {
	window_set_fullscreen(option_value[? "Fullscreen"]);
	fullscreen_change = false;	
	// Update graphics
	fnc_SetGraphics();
	
}
else if (keyboard_check_pressed(vk_f10)) {
	option_value[? "Fullscreen"] = window_get_fullscreen();	
	// Update graphics
	fnc_SetGraphics();
}

// GUI Size
if (window_get_fullscreen()) {
	display_set_gui_size(display_get_width(), display_get_height());
	//show_debug_message("Full screen: GUI is now "+string(display_get_gui_width())+"x"+string(display_get_gui_height()));
	//display_set_gui_maximize(1, 1, 0, 0);
}
else {		
	display_set_gui_size(window_get_width(), window_get_height());
	//show_debug_message("Windowed: GUI is now "+string(display_get_gui_width())+"x"+string(display_get_gui_height()));
	//show_debug_message(string(window_get_width())+" "+string(window_get_height()));
}

if (!Game.paused) {	
	
	if (room == room_Game_1) {
		
		// Get current camera position
		var _current_camera_x = camera_get_view_x(VIEW);
		var _current_camera_y = camera_get_view_y(VIEW);
	
		// Calculate offset for screen shake			
		var _offsetx = 0;
		var _offsety = 0;
		if (camera_shake) {
			var _offsetx = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
			var _offsety = irandom_range(-SCREENSHAKE_STRENGTH,SCREENSHAKE_STRENGTH);
		}
		
		if (active_player != noone) {
			var _destx = clamp(active_player.x-adjusted_camera_width/2, 0, room_width-adjusted_camera_width);
			var _desty = clamp(active_player.y-adjusted_camera_height/6, 0, room_height-adjusted_camera_height);			
		}
		else if (instance_number(obj_Player) == 1) {
			var _destx = clamp(obj_Player.x-adjusted_camera_width/2, 0, room_width-adjusted_camera_width);
			var _desty = clamp(obj_Player.y-adjusted_camera_height/2, 0, room_height-adjusted_camera_height);				
		}
		else {
			var _destx = _current_camera_x;
			var _desty = _current_camera_y;
		}
		
		var _target_camera_x = lerp(_current_camera_x, _destx, camera_smoothness) + _offsetx;
		var _target_camera_y = lerp(_current_camera_y, _desty, camera_smoothness) + _offsety;
			
		// Move camera
		camera_set_view_pos(VIEW, _target_camera_x, _target_camera_y);

	}
	
}