/// @description 
option_value[? "Music"] = !option_value[? "Music"];
option_value[? "Sounds"] = !option_value[? "Sounds"];

if (option_value[? "Music"]) {
	show_debug_message("Music and sound on");
	if (audio_is_paused(music_sound_id)) {
		audio_resume_sound(music_sound_id);	
	}
}
else {
	show_debug_message("Music and sound off");
	if (audio_is_playing(music_sound_id)) {
		audio_pause_sound(music_sound_id);	
	}
}