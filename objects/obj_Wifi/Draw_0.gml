/// @description 
if (!Game.paused) {
	fnc_BackupDrawParams();	
	//draw_rectangle(bbox_left, bbox_top, bbox_right, bbox_bottom, true);
	
	draw_set_color(c_white);
	draw_set_font(fnt_Icons);
	draw_set_halign(fa_center);
	draw_set_valign(fa_middle);
	
	if (alarm[0] != -1) {
		if (alarm[0] == 60) {
			Game.score_bonus = Game.score_bonus + 100;
		}
		var _scale = 1/5 + 1/5*(1-alarm[0]/60);
		draw_set_alpha(alarm[0]/60);
		draw_text_transformed(x, y, icon_type_on[index], _scale, _scale, 0);	
	}
	else {
		var _scale = 1/5;
		draw_set_alpha(1);
		draw_text_transformed(x, y, icon_type_off[index], _scale, _scale, 0);
	}
	fnc_RestoreDrawParams();
}