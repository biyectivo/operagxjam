/// @description 

if (!Game.paused) {
	// Destroy outside of view or hp nonpositive
	var _top = camera_get_view_y(VIEW) - 2*TILE_SIZE;
	var _bottom = camera_get_view_y(VIEW) + camera_get_view_height(VIEW) + 2*TILE_SIZE;
	if (x < 0 || x > room_width || y < _top || y > _bottom) {
		instance_destroy();
	}
}