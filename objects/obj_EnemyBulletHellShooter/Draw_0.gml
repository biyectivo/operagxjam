/// @description Draw agent
if (!Game.paused) {
	if (alarm[11] != -1) {
		shader_set(shd_HitFlash);
	}
	draw_sprite_ext(spr_Browsers, enemy_number, x, y, 1, 1, image_angle, c_white, 1);
	draw_self();
	shader_reset();
}