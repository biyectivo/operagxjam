/// @description Draw agent
if (!Game.paused) {
	if (timeout) {
		
		if (hp > 0) {
			// Shoot
			
			if (enemy_number == 3) {
				if (Game.active_player != noone && alarm[1] == -1) {
					for (var _i=0; _i<8; _i++) {
						var _angle = 360/8*_i;
						var _id = instance_create_layer(x+TILE_SIZE/2*cos(_angle), y-TILE_SIZE/2*sin(_angle), "lyr_Instances", obj_Bullet);
						with (_id) {
							shot_by = other.id;
							bullet_angle = _angle;
							bullet_speed = 1.5;
							image_angle = _angle-90;
							image_index = 1;
						}
					}
					alarm[1] = 60;
				}
			}
			else {
				if (Game.active_player != noone && alarm[1] == -1) {
					for (var _i=0; _i<6; _i++) {
						var _angle = 360/6*_i;
						var _id = instance_create_layer(x+TILE_SIZE/2*cos(_angle), y-TILE_SIZE/2*sin(_angle), "lyr_Instances", obj_Bullet);
						with (_id) {
							shot_by = other.id;
							bullet_angle = _angle;
							bullet_speed = 1.5;
							image_angle = _angle-90;
							image_index = 1;
						}
					}
					alarm[1] = 40;
				}
			}
			
	
			// Change angle
			if (x>xprevious) {			
				var _target = -40;
			}
			else if (x<xprevious) {
				var _target = 40
			}
			else {
				var _target = 0;	
			}
			
			var _curve = animcurve_get_channel(CSSTransitions, "ease-in-cubic");
			var _amount = animcurve_channel_evaluate(_curve, min(steps/60, 1));		
			image_angle = lerp(image_angle, _target, _amount);
		}
		
		// Destroy outside of view or hp nonpositive
		var _top = camera_get_view_y(VIEW) - 2*TILE_SIZE;
		var _bottom = camera_get_view_y(VIEW) + camera_get_view_height(VIEW) + 2*TILE_SIZE;
		if (x < 0 || x > room_width || y < _top || y > _bottom) {
			instance_destroy();
		}
		event_inherited();
	}
}