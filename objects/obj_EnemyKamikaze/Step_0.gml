/// @description Draw agent
if (!Game.paused) {
	var _id = instance_nearest(x, y, obj_Player);
	
	if (hp > 0) {
		// Kamikaze
		if (y >= _id.y) {
			current_angle = point_direction(x, y, _id.x, _id.y);			
		}
		var _x = lengthdir_x(current_hspeed, current_angle);
		var _y = lengthdir_y(current_vspeed, current_angle);
		x = x + _x;
		y = y + _y;
	
	
		// Change angle
		if (x>xprevious) {			
			var _target = -40;
		}
		else if (x<xprevious) {
			var _target = 40
		}
		else {
			var _target = 0;	
		}
			
		var _curve = animcurve_get_channel(CSSTransitions, "ease-in-cubic");
		var _amount = animcurve_channel_evaluate(_curve, min(steps/60, 1));		
		image_angle = lerp(image_angle, _target, _amount);
	}
	
	// Destroy outside of view or hp nonpositive
	var _top = camera_get_view_y(VIEW) - 2*TILE_SIZE;
	var _bottom = camera_get_view_y(VIEW) + camera_get_view_height(VIEW) + 2*TILE_SIZE;
	if (x < 0 || x > room_width || y < _top || y > _bottom) {
		instance_destroy();
	}
	event_inherited();
	
}