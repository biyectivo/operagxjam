/// @description
//if (live_call()) return live_result;
fnc_BackupDrawParams();
draw_set_font(fnt_Title);
draw_set_valign(fa_top);
draw_set_halign(fa_center);
draw_set_color(c_blue);
draw_text(window_get_width()/2+3, 10+3, "'Net Descent");
draw_set_color(c_white);
draw_text(window_get_width()/2, 10, "'Net Descent");

draw_set_font(fnt_HUD);
draw_text(window_get_width()/2, 75, "The alien browsers have abducted the Internet!");	

draw_set_color(c_gray);
draw_set_alpha(0.8);
/*draw_text(window_get_width()/2, 130, "Lead OperaGX and his pal, Opera, to the icy giant planet of");
draw_text(window_get_width()/2, 150, "Browsrium, descend as deeply as possible and");
draw_text(window_get_width()/2, 170, "recover any internet nodes on your way!");*/
type_formatted(window_get_width()/2, 110, "[fnt_HUD][fa_center][fa_top][c_gray]Lead [c_red]OperaGX[c_gray] and his pal, [c_red]Opera[c_gray], to the icy giant planet of");
type_formatted(window_get_width()/2, 130, "[fnt_HUD][fa_center][fa_top][c_gray]Browsrium, descend as deeply as possible and");
type_formatted(window_get_width()/2, 150, "[fnt_HUD][fa_center][fa_top][c_gray]collect [scale,0.2][fnt_Icons][c_white]"+ICON_WIFIOFF_1+ICON_WIFIOFF_2+ICON_WIFIOFF_3+ICON_WIFIOFF_4+ICON_WIFIOFF_5+"[c_gray][fnt_HUD][scale,1] on your way!");

draw_set_alpha(1);
// 1
var _x = window_get_width()/2-140;
var _y = window_get_height()/2+70;
var _x1 = _x-40;
var _y1 = _y-40;
var _x2 = _x+40;
var _y2 = _y+40;


draw_roundrect_color(_x1, _y1, _x2, _y2, $333333, $333333, true );
draw_sprite_ext(spr_Browsers, 0, _x, _y, 1, 1,0, c_white, 1);
draw_sprite_ext(spr_UFO, -1, _x, _y, 1, 1, 0, c_white, 1);

// 2
var _x = window_get_width()/2+140;
var _y = window_get_height()/2+70;
var _x1 = _x-40;
var _y1 = _y-40;
var _x2 = _x+40;
var _y2 = _y+40;


draw_roundrect_color(_x1, _y1, _x2, _y2, $333333, $333333, true );
draw_sprite_ext(spr_Browsers, 1, _x, _y, 1, 1,0, c_white, 1);
draw_sprite_ext(spr_UFO, -1, _x, _y, 1, 1, 0, c_white, 1);

// controls
draw_set_halign(fa_right);
var _x = window_get_width()/2-224;
var _y = window_get_height()/2+50;
draw_text(_x, _y, "left");
draw_sprite_ext(spr_Keys, 0, _x+12, _y, 1, 1, 0, c_white, 1);
draw_text(_x, _y+16, "shoot");
draw_sprite_ext(spr_Keys, 1, _x+12, _y+16, 1, 1, 0, c_white, 1);
draw_text(_x, _y+32, "right");
draw_sprite_ext(spr_Keys, 2, _x+12, _y+32, 1, 1, 0, c_white, 1);

draw_set_halign(fa_left);
var _x = window_get_width()/2+224;
var _y = window_get_height()/2+50;
draw_text(_x, _y, "left");
draw_sprite_ext(spr_Keys, 3, _x-24, _y, 1, 1, 0, c_white, 1);
draw_text(_x, _y+16, "shoot");
draw_sprite_ext(spr_Keys, 4, _x-24, _y+16, 1, 1, 0, c_white, 1);
draw_text(_x, _y+32, "right");
draw_sprite_ext(spr_Keys, 5, _x-24, _y+32, 1, 1, 0, c_white, 1);
fnc_RestoreDrawParams();


if (keyboard_check_pressed(vk_right)) {
	Game.num_players = 2;
}
if (keyboard_check_pressed(vk_left)) {
	Game.num_players = 1;
}

var _x2letter = window_get_width()/2+300;
if (Game.num_players == 1) {
	var _x2 = window_get_width()/2-50;
}
else {
	var _x2 = _x2letter;
}
	

draw_roundrect_color_ext(window_get_width()/2-300, window_get_height()/2+10 , _x2, window_get_height()/2+130, 5, 5, c_white, c_white, true);
draw_roundrect_color_ext(window_get_width()/2-300, window_get_height()/2+140 , _x2letter, window_get_height()/2+170, 5, 5, c_gray, c_gray, false);
draw_roundrect_color_ext(window_get_width()/2-300, window_get_height()/2+140 , _x2letter, window_get_height()/2+170, 5, 5, c_black, c_black, true);

type_formatted(window_get_width()/2, window_get_height()/2+155, "[fnt_HUD][fa_center][fa_middle][c_navy]ENTER to play "+(Game.num_players==1 ? "single player" : "two player co-op")+"  [c_aqua]HI SCORE: "+string((Game.num_players == 1 ? Game.high_score_1p : Game.high_score_2p)));