/// @description 


// High score
if (!file_exists("high_score.dat")) {
	fnc_UpdateHighScore(2, true); // force creation of file
}
else {// Read high score file
	var _fid = file_text_open_read("high_score.dat");
	var _str = file_text_readln(_fid);
	var _split = fnc_StringToList(_str, ";");
		
	var _split_player_hash = fnc_StringToList(_split[|0], "|");
	var _split_player = fnc_StringToList(_split_player_hash[|0], " ");
		
	// 1 Player
	if (sha1_string_utf8(SCOREBOARD_SALT+_split_player[|0]+" "+_split_player[|1]+" "+_split_player[|2]+" "+_split_player[|3]+" "+_split_player[|4]) == _split_player_hash[|1]) {
		Game.high_score_1p = real(_split_player[|3]);
		Game.high_score_distance_1p = real(_split_player[|4]);
		Game.high_score_datetime_string_1p = _split_player[|0]+" "+_split_player[|1];
	}
	else {
		Game.high_score_1p = -1;
		Game.high_score_distance_1p = -1;			
	}
		
	// 2 Player
	var _split_player_hash = fnc_StringToList(_split[|1], "|");
	var _split_player = fnc_StringToList(_split_player_hash[|0], " ");
		
	if (sha1_string_utf8(SCOREBOARD_SALT+_split_player[|0]+" "+_split_player[|1]+" "+_split_player[|2]+" "+_split_player[|3]+" "+_split_player[|4]) == _split_player_hash[|1]) {
		Game.high_score_2p = real(_split_player[|3]);
		Game.high_score_distance_2p = real(_split_player[|4]);
		Game.high_score_datetime_string_2p = _split_player[|0]+" "+_split_player[|1];
	}
	else {
		Game.high_score_2p = -1;
		Game.high_score_distance_2p = -1;			
	}
	file_text_close(_fid)
}