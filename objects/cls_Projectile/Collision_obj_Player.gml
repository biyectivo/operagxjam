/// @description 
if (!Game.paused) {
	var _n = instance_number(obj_Player); 
	var _shot_by_player = false;
	var _i=0;
	while (_i<_n && !_shot_by_player) {
		_id = instance_find(obj_Player, _i);
		if (shot_by == _id.id) {
			_shot_by_player = true;
		}
		else {
			_i++;	
		}
	}
	if (!_shot_by_player) {
		other.hp--;
		other.alarm[11] = 3;
		instance_destroy();
	}
}