function fnc_PlayerFSM_AddGravity() {
	////if (live_call()) return live_result;
	current_fly_vspeed = min(descent_speed * multiplier, max_descent_speed);
}

/// @function fnc_PlayerFSM_Idle
/// @description Idle state for player
function fnc_PlayerFSM_Idle() {
	////if (live_call()) return live_result;

	// Add deceleration
	if (current_fly_hspeed > 0) {
		current_fly_hspeed = max(current_fly_hspeed - max_fly_hdecel* multiplier, 0);
		fnc_PlayerFSM_ProcessMove_Horizontal()
	}
	else if (current_fly_hspeed < 0) {
		current_fly_hspeed = min(current_fly_hspeed + max_fly_hdecel* multiplier, 0);
		fnc_PlayerFSM_ProcessMove_Horizontal()
	}
	
	// Gravity
	fnc_PlayerFSM_AddGravity();	
	fnc_PlayerFSM_ProcessMove_Vertical();
	
	// Special
	if (player_vertical_input) {		
		fnc_PlayerFSM_Special();
	}
	
	// Get Input
	fnc_PlayerFSM_GetInput();
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
	
}

function fnc_PlayerFSM_ProcessMove_Horizontal() {
	////if (live_call()) return live_result;
	
	// Via keys - horizontal	
	var _commit_speed;
	
	// Normal move	
	
	var _collision_pos = (current_fly_hspeed > 0 ? bbox_right : bbox_left);
		
	if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + current_fly_hspeed, y) != 1
			&& tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + current_fly_hspeed, bbox_top+1) != 1
			&& tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + current_fly_hspeed, bbox_bottom-1) != 1
	) {
		_commit_speed = current_fly_hspeed;
	}
	else {		
		/*show_debug_message(
				"Horizontal:  "+string(current_fly_hspeed)+
				string(tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + current_fly_hspeed, y))
			+" "+string(tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + current_fly_hspeed, bbox_top+1))
			+" "+string(tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), _collision_pos + current_fly_hspeed, bbox_bottom-1))
		);*/
		current_fly_hspeed = 0;
		_commit_speed = 0;
		hp = 0;
	}
		
	// Check for collisions with collisionable objects and snap to the bounding box if needed
	if (place_meeting(x + _commit_speed, y, cls_Collisionable)) {
		var _id = instance_place(x + _commit_speed, y, cls_Collisionable);
		
		switch (object_get_name(_id.object_index)) {
			case "obj_Player":
				x = x + _commit_speed;
				break;
			default:
				while (!place_meeting(x + sign(_commit_speed), y, cls_Collisionable)) {
					x = x + sign(_commit_speed);
				}
				current_fly_hspeed = 0;				
				hp = 0;
				break;
		}
	}
	else {
		x = x + _commit_speed;
	}
		
}

function fnc_PlayerFSM_ProcessMove_Vertical() {
	////if (live_call()) return live_result;
	
	// Via keys - vertical
	var _commit_speed;
	
	// Normal move	
	
	var _collision_pos = (current_fly_vspeed > 0 ? bbox_bottom : bbox_top);
		
	if (tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), x, _collision_pos + current_fly_vspeed) != 1
		&& tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), bbox_left+1, _collision_pos + current_fly_vspeed) != 1
		&& tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), bbox_right-1, _collision_pos + current_fly_vspeed) != 1
	) {
		_commit_speed = current_fly_vspeed;
	}
	else {
		/*show_debug_message(
				"Vertical:  "+string(current_fly_hspeed)+
				string(tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), x, _collision_pos + current_fly_vspeed))
		+" "+string(tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), bbox_left+1, _collision_pos + current_fly_vspeed))
		+" "+string(tilemap_get_at_pixel(layer_tilemap_get_id(layer_get_id("lyr_Tile_Collision")), bbox_right-1, _collision_pos + current_fly_vspeed))
		);*/
		current_fly_vspeed = 0;
		_commit_speed = 0;
		hp = 0;
	}
		
	// Check for collisions with collisionable objects and snap to the bounding box if needed
	if (place_meeting(x, y + _commit_speed, cls_Collisionable)) {			
		var _id = instance_place(x, y + _commit_speed, cls_Collisionable);						
		
		switch (object_get_name(_id.object_index)) {
			case "obj_Player":
				y = y + _commit_speed;
				break;
			default:
				while (!place_meeting(x, y + sign(_commit_speed), cls_Collisionable)) {
					y = y + sign(_commit_speed);
				}
				current_fly_vspeed = 0;
				hp = 0;
				break;
		}
			
	}
	else {
		y = y + _commit_speed;
	}	
		
	
}

/// @function fnc_PlayerFSM_Move
/// @description Move state for player
function fnc_PlayerFSM_Move() {
	////if (live_call()) return live_result;
	
	
	// Add movement
	if (player_horizontal_input == 1) {
		current_fly_hspeed = min(current_fly_hspeed + max_fly_haccel* multiplier, max_fly_hspeed);
	}
	else if (player_horizontal_input == -1) {
		current_fly_hspeed = max(current_fly_hspeed - max_fly_haccel* multiplier, -max_fly_hspeed);
	}
	
	// Add Gravity 
	fnc_PlayerFSM_AddGravity();
	fnc_PlayerFSM_ProcessMove_Vertical();
	
	// Perform moves
	fnc_PlayerFSM_ProcessMove_Horizontal();	
	
	// Special
	if (player_vertical_input) {		
		fnc_PlayerFSM_Special();
	}
	
	// Update facing
	if (player_horizontal_input != 0) {
		facing = (player_horizontal_input == 1 ? FACING.EAST : FACING.WEST);
	}
	
	
	// Get Input
	fnc_PlayerFSM_GetInput();
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
	
}

function fnc_PlayerFSM_Special() {
	
	// Bullet
	if (alarm[1] == -1) { 
		var _id = instance_create_layer(x, y+TILE_SIZE/2, "lyr_Instances", obj_Bullet);
		with (_id) {
			shot_by = other.id;
			bullet_angle = 270;
			bullet_speed = other.current_fly_vspeed + 5;
			image_index = 0;
		}
		alarm[1] = 10;
	}
	
}

function fnc_PlayerFSM_Die() {
	/*facing = FACING.EAST;
	if (obj_Player.alarm[3] == -1) {
		obj_Player.alarm[3] = (animation_lengths[? "Die"]-1) * animation_speeds[? "Die"];		
	}*/
	
	if (alarm[3] == -1) { // create fragments
		var _num_fragments = 6;
		for (var _i=0; _i<_num_fragments; _i++) {
			for (var _j=0; _j<_num_fragments; _j++) {
				var _id = instance_create_layer(x, y, "lyr_Instances", obj_Fragment);
				with (_id) {
					sprite_index = spr_Browsers;
					image_index = other.player_number;
					fragment_w = sprite_width/_num_fragments;
					fragment_h = sprite_height/_num_fragments;
					fragment_index_w = _i;
					fragment_index_h = _j;
					event_perform(ev_other, ev_user0);
				}				
			}
		}
		Game.camera_shake = true;
		Game.alarm[1] = 10;
		if (Game.option_value[? "Sounds"]) {			
			audio_play_sound(snd_Explosion, 1, false);
		}
		visible = false;
		controllable = false;
		alarm[3] = obj_Fragment.fragment_duration+1;
	}		
	
	// Transition to other states
	fnc_PlayerFSM_Transition();
}

function fnc_PlayerFSM_GetInput() {
	////if (live_call()) return live_result;
	
	//show_debug_message("Horizontal speed: "+string_format(current_fly_hspeed, 8,4)+"   Vertical speed: "+string_format(current_fly_vspeed, 8, 4));

	
	// Detect gamepad
	/*
	var _maxpads = gamepad_get_device_count();
	Game.primary_gamepad = -1;
	
	var _i=0; 
	while (_i < _maxpads && Game.primary_gamepad == -1) {
		Game.primary_gamepad = gamepad_is_connected(_i) ? _i : -1;
		_i++;
	}
	*/
	
	// Keyboard - Player 1
	player_horizontal_input = keyboard_check(Game.controls[player_number][? "right"]) - keyboard_check(Game.controls[player_number][? "left"]);
	player_vertical_input = keyboard_check(Game.controls[player_number][? "down"]);
	
}

function fnc_PlayerFSM_Animate(_reset_animation) {	
	////if (live_call()) return live_result;
	
	animation_frame_count = _reset_animation ? 0 : ((animation_frame_count + 1) % animation_lengths[? animation_name]);
	animation_frame = animation_frame_count * animation_spacings[? animation_name];
	if (animation_dirs >= 4) {
		image_index = animation_startindices[? animation_name] + facing * animation_lengths[? animation_name] + animation_frame;		
		if  (Game.debug) {
			show_debug_message(name+" @ "+state+" facing "+string(facing)+": ("+string(_reset_animation)+") "+string(animation_startindices[? animation_name])+"+"+string(facing)+"*"+string(animation_lengths[? animation_name])+"+"+string(animation_frame)+"="+string(image_index));
		}
	}
	else if (animation_dirs	== 2) {		
		image_xscale = (facing == FACING.EAST ? -base_xscale : base_xscale);			
		image_index = player_horizontal_input == -1 ? 1 : (player_horizontal_input == 1 ? 2 : 0);			
	}
	else {
		image_index = animation_startindices[? animation_name] + animation_frame;
	}
	
}

function fnc_PlayerFSM_ResetAnimation() {
	fnc_PlayerFSM_Animate(true);
	alarm[0] = animation_speeds[? animation_name];
}

function fnc_PlayerFSM_SmoothRotate() {
	////if (live_call()) return live_result;
	
	if (state != "Die") {
		if (state == "Move") {
			if (current_fly_hspeed > 0) {			
				var _target = -40;
			}
			else if (current_fly_hspeed < 0) {
				var _target = 40
			}
			else {
				var _target = 0;	
			}
		}	
		else if (state == "Idle") {		
			var _target = 0;
		}
		
		
		var _curve = animcurve_get_channel(CSSTransitions, "ease-in-cubic");
		var _amount = animcurve_channel_evaluate(_curve, min(steps_in_state/60, 1));
		
		image_angle = lerp(image_angle, _target, _amount);
	}
}

function fnc_PlayerFSM_Transition() {
	////if (live_call()) return live_result;
	
	last_state = state;	
	
	// Transition matrix	
	if (state == "Idle") {		
		if (hp <= 0) {
			state = "Die";		
		}
		else if (controllable && player_horizontal_input != 0) {						
			state = "Move";
		}
		else {			
			state = "Idle";		
		}
	}
	else if (state == "Move") {
		if (hp <= 0) {
			state = "Die";		
		}
		else if (player_horizontal_input == 0) {
			state = "Idle";
		}
		else {
			state = "Move";
		}
	}
	else { // Die
		
	}
	
	// Reset animation
	reset_animation = (state != last_state);
	animation_name = state;
	
	if (reset_animation) {
		steps_in_state = 0;
	}
	else {
		steps_in_state++;
	}
	/*if (reset_animation) {
		if (Game.debug) {
			show_debug_message(name+" transitioning from "+last_state+" to "+state);
		}
		fnc_PlayerFSM_ResetAnimation();
	}*/
}