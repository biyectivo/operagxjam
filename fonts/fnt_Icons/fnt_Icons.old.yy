{
  "hinting": 0,
  "glyphOperations": 0,
  "interpreter": 0,
  "pointRounding": 0,
  "fontName": "Material Icons Sharp",
  "styleName": "Regular",
  "size": 96.0,
  "bold": false,
  "italic": false,
  "charset": 0,
  "AntiAlias": 1,
  "first": 0,
  "last": 0,
  "sampleText": "abcdef ABCDEF\n0123456789 .,<>\"'&!?\nthe quick brown fox jumps over the lazy dog\nTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\nDefault character: ▯ (9647)",
  "includeTTF": false,
  "TTFName": "",
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "ascenderOffset": 0,
  "glyphs": {
    "32": {"x":2,"y":2,"w":128,"h":128,"character":32,"shift":128,"offset":0,},
    "57396": {"x":733,"y":132,"w":64,"h":128,"character":57396,"shift":128,"offset":32,},
    "57410": {"x":645,"y":132,"w":86,"h":128,"character":57410,"shift":128,"offset":21,},
    "57423": {"x":545,"y":132,"w":98,"h":128,"character":57423,"shift":128,"offset":15,},
    "57424": {"x":447,"y":132,"w":96,"h":128,"character":57424,"shift":128,"offset":16,},
    "57550": {"x":337,"y":132,"w":108,"h":128,"character":57550,"shift":128,"offset":10,},
    "57816": {"x":211,"y":132,"w":124,"h":128,"character":57816,"shift":128,"offset":2,},
    "57818": {"x":85,"y":132,"w":124,"h":128,"character":57818,"shift":128,"offset":2,},
    "57826": {"x":799,"y":132,"w":108,"h":128,"character":57826,"shift":128,"offset":10,},
    "57858": {"x":2,"y":132,"w":81,"h":128,"character":57858,"shift":128,"offset":26,},
    "58045": {"x":713,"y":2,"w":128,"h":128,"character":58045,"shift":128,"offset":0,},
    "58049": {"x":583,"y":2,"w":128,"h":128,"character":58049,"shift":128,"offset":0,},
    "58373": {"x":517,"y":2,"w":64,"h":128,"character":58373,"shift":128,"offset":32,},
    "58378": {"x":407,"y":2,"w":108,"h":128,"character":58378,"shift":128,"offset":10,},
    "58432": {"x":308,"y":2,"w":97,"h":128,"character":58432,"shift":128,"offset":16,},
    "58821": {"x":252,"y":2,"w":54,"h":128,"character":58821,"shift":128,"offset":37,},
    "58942": {"x":132,"y":2,"w":118,"h":128,"character":58942,"shift":128,"offset":5,},
    "58952": {"x":843,"y":2,"w":118,"h":128,"character":58952,"shift":128,"offset":5,},
    "61492": {"x":909,"y":132,"w":106,"h":128,"character":61492,"shift":128,"offset":7,},
  },
  "kerningPairs": [],
  "ranges": [
    {"lower":57396,"upper":57396,},
    {"lower":57410,"upper":57410,},
    {"lower":57423,"upper":57424,},
    {"lower":57550,"upper":57550,},
    {"lower":57816,"upper":57816,},
    {"lower":57818,"upper":57818,},
    {"lower":57826,"upper":57826,},
    {"lower":57858,"upper":57858,},
    {"lower":58045,"upper":58045,},
    {"lower":58049,"upper":58049,},
    {"lower":58373,"upper":58373,},
    {"lower":58378,"upper":58378,},
    {"lower":58432,"upper":58432,},
    {"lower":58821,"upper":58821,},
    {"lower":58942,"upper":58942,},
    {"lower":58952,"upper":58952,},
    {"lower":61492,"upper":61492,},
  ],
  "regenerateBitmap": false,
  "canGenerateBitmap": true,
  "maintainGms1Font": false,
  "parent": {
    "name": "Fonts",
    "path": "folders/Fonts.yy",
  },
  "resourceVersion": "1.0",
  "name": "fnt_Icons",
  "tags": [],
  "resourceType": "GMFont",
}